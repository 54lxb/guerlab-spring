package net.guerlab.spring.searchparams;

import java.lang.annotation.*;

/**
 * 搜索模式
 *
 * @author guer
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SearchModel {

    /**
     * 默认搜索模式
     */
    SearchModel DEFAULT = new SearchModel() {

        @Override
        public Class<? extends Annotation> annotationType() {
            return SearchModel.class;
        }

        @Override
        public SearchModelType value() {
            return SearchModelType.EQUAL_TO;
        }

        @Override
        public String sql() {
            return null;
        }
    };

    /**
     * 获取搜索模式类型
     *
     * @return 搜索模式类型
     */
    SearchModelType value() default SearchModelType.EQUAL_TO;

    /**
     * 自定义SQL
     *
     * @return 自定义SQL
     */
    String sql() default "";
}
