package net.guerlab.spring.searchparams;

/**
 * SearchParams参数处理接口
 *
 * @author guer
 *
 */
@FunctionalInterface
public interface SearchParamsHandler {

    /**
     * 设置参数值
     *
     * @param object
     *            输出对象
     * @param fieldName
     *            类字段名
     * @param columnName
     *            数据库字段名
     * @param value
     *            参数值
     * @param searchModel
     *            搜索模式
     */
    void setValue(final Object object, final String fieldName, final String columnName, final Object value,
            final SearchModel searchModel);
}
