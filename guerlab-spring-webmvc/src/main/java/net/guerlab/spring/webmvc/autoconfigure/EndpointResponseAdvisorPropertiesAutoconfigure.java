package net.guerlab.spring.webmvc.autoconfigure;

import net.guerlab.spring.webmvc.properties.ResponseAdvisorProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guer
 *
 */
@Configuration
@ConditionalOnBean(WebEndpointProperties.class)
@EnableConfigurationProperties(ResponseAdvisorProperties.class)
public class EndpointResponseAdvisorPropertiesAutoconfigure {

    @Autowired
    public void advisor(WebEndpointProperties webEndpointProperties,
            ResponseAdvisorProperties responseAdvisorProperties) {
        String basePath = StringUtils.trimToNull(webEndpointProperties.getBasePath());

        if (basePath == null) {
            return;
        }

        List<String> excluded = responseAdvisorProperties.getExcluded();

        List<String> list = excluded == null ? new ArrayList<>() : new ArrayList<>(excluded);
        list.add(basePath);

        responseAdvisorProperties.setExcluded(list);
    }
}
