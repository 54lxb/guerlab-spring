package net.guerlab.spring.webmvc.properties;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.cors.CorsConfiguration;

/**
 * CORS配置
 *
 * @author guer
 *
 */
@RefreshScope
@ConditionalOnProperty(name = "guerlab.web.mvc.cors.enable", havingValue = "true")
@ConfigurationProperties("guerlab.web.mvc.cors")
public class CorsProperties extends CorsConfiguration {

}
