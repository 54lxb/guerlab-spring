package net.guerlab.spring.webflux.autoconfigure;

import net.guerlab.web.result.Result;
import org.springframework.web.reactive.function.server.ServerRequest;

/**
 * 全局异常处理器
 *
 * @author guer
 */
public interface IGlobalExceptionHandler {

    /**
     * 处理异常信息
     *
     * @param request
     *         请求
     * @param error
     *         异常
     * @return 异常返回信息
     */
    Result<?> handler(ServerRequest request, Throwable error);
}
