package net.guerlab.spring.webflux.autoconfigure;

import net.guerlab.commons.exception.ApplicationException;
import net.guerlab.spring.commons.exception.*;
import net.guerlab.spring.webflux.exception.NoHandlerFoundExceptionInfo;
import net.guerlab.web.result.Fail;
import net.guerlab.web.result.Result;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * 默认全局异常处理器
 *
 * @author guer
 */
public class DefaultGlobalExceptionHandler implements IGlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGlobalExceptionHandler.class);

    private static final int HTTP_STATUS_VALUE_NOT_FOUND = 404;

    private MessageSource messageSource;

    public DefaultGlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public Result<?> handler(ServerRequest request, Throwable error) {
        if (error == null) {
            return new Fail<>();
        } else if (error instanceof ResponseStatusException) {
            return responseStatusException(request, (ResponseStatusException) error);
        } else if (error instanceof MethodArgumentTypeMismatchException) {
            return methodArgumentTypeMismatchException(request, (MethodArgumentTypeMismatchException) error);
        } else if (error instanceof MethodArgumentNotValidException) {
            return methodArgumentNotValidException(request, (MethodArgumentNotValidException) error);
        } else if (error instanceof ConstraintViolationException) {
            return constraintViolationException(request, (ConstraintViolationException) error);
        } else if (error instanceof AbstractI18nApplicationException) {
            return abstractI18nApplicationException(request, (AbstractI18nApplicationException) error);
        } else if (error instanceof ApplicationException) {
            return applicationException(request, (ApplicationException) error);
        } else {
            return exception(request, error);
        }
    }

    private Fail<Void> responseStatusException(ServerRequest request, ResponseStatusException e) {
        debug(request, e);

        HttpStatus httpStatus = e.getStatus();
        int httpStatusValue = httpStatus.value();
        if (httpStatusValue == HTTP_STATUS_VALUE_NOT_FOUND) {
            return handler0(new NoHandlerFoundExceptionInfo(e, request.methodName(), request.path()), e);
        } else {
            return handler0(new DefaultExceptionInfo(e), e);
        }
    }

    private Fail<Void> methodArgumentTypeMismatchException(ServerRequest request,
            MethodArgumentTypeMismatchException e) {
        debug(request, e);
        return handler0(new MethodArgumentTypeMismatchExceptionInfo(e), e);
    }

    private Fail<Collection<String>> methodArgumentNotValidException(ServerRequest request,
            MethodArgumentNotValidException e) {
        debug(request, e);

        BindingResult bindingResult = e.getBindingResult();

        Collection<String> displayMessageList = bindingResult.getAllErrors().stream()
                .map(this::getMethodArgumentNotValidExceptionDisplayMessage).collect(Collectors.toList());

        return handler0(new RequestParamsError(e, displayMessageList));
    }

    private String getMethodArgumentNotValidExceptionDisplayMessage(ObjectError error) {
        String defaultMessage = error.getDefaultMessage();

        Locale locale = LocaleContextHolder.getLocale();

        try {
            /*
             * use custom message
             */
            return messageSource.getMessage(defaultMessage, null, locale);
        } catch (NoSuchMessageException e) {
            /*
             * use ObjectError default message
             */
            return error.getObjectName() + error.getDefaultMessage();
        }
    }

    private Fail<Collection<String>> constraintViolationException(ServerRequest request,
            ConstraintViolationException e) {
        debug(request, e);

        Collection<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();

        Collection<String> displayMessageList = constraintViolations.stream().map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());

        return handler0(new RequestParamsError(e, displayMessageList));
    }

    private Fail<Void> abstractI18nApplicationException(ServerRequest request, AbstractI18nApplicationException e) {
        debug(request, e);

        String message = e.getMessage(messageSource);

        LOGGER.debug(message, e);

        return new Fail<>(message, e.getErrorCode());
    }

    private Fail<Void> applicationException(ServerRequest request, ApplicationException e) {
        debug(request, e);

        String message = getMessage(e.getLocalizedMessage());

        LOGGER.debug(message, e);

        return new Fail<>(message, e.getErrorCode());
    }

    private Fail<Void> exception(ServerRequest request, Throwable e) {
        debug(request, e);

        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class);

        if (responseStatus != null) {
            int errorCode = responseStatus.value().value();
            String message = responseStatus.reason();

            return new Fail<>(getMessage(message), errorCode);
        } else if (StringUtils.isBlank(e.getMessage())) {
            return handler0(new DefaultExceptionInfo(e), e);
        } else {
            return new Fail<>(getMessage(e.getLocalizedMessage()));
        }
    }

    private void debug(ServerRequest request, Throwable e) {
        LOGGER.debug(String.format("request uri[%s %s]", request.method(), request.path()), e);
    }

    private Fail<Collection<String>> handler0(RequestParamsError e) {
        String message = getMessage(e.getLocalizedMessage());

        LOGGER.debug(message, e);

        Fail<Collection<String>> fail = new Fail<>(message, e.getErrorCode());

        fail.setData(e.getErrors());

        return fail;
    }

    private Fail<Void> handler0(AbstractI18nInfo i18nInfo, Throwable e) {
        String message = i18nInfo.getMessage(messageSource);

        LOGGER.debug(message, e);

        return new Fail<>(message, i18nInfo.getErrorCode());
    }

    private String getMessage(String msg) {
        if (StringUtils.isBlank(msg)) {
            return msg;
        }

        Locale locale = LocaleContextHolder.getLocale();

        return messageSource.getMessage(msg, null, msg, locale);
    }
}
