package net.guerlab.spring.webflux.autoconfigure;

import net.guerlab.web.result.Fail;
import net.guerlab.web.result.Result;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * 全局错误处理
 *
 * @author guer
 */
public class GlobalErrorWebExceptionHandler extends DefaultErrorWebExceptionHandler {

    private IGlobalExceptionHandler globalExceptionHandler;

    public GlobalErrorWebExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties,
            ErrorProperties errorProperties, ApplicationContext applicationContext,
            IGlobalExceptionHandler globalExceptionHandler) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
        this.globalExceptionHandler = globalExceptionHandler;
    }

    @Override
    protected Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {
        Throwable error = super.getError(request);

        Result<?> result = globalExceptionHandler.handler(request, error);

        Map<String, Object> errorAttributes = new HashMap<>(4);
        errorAttributes.put("status", result.isStatus());
        errorAttributes.put("errorCode", result.getErrorCode());
        String message = result.getMessage();
        errorAttributes.put("message", message != null ? message : new Fail<>().getMessage());
        Object data = result.getData();
        if (data != null) {
            errorAttributes.put("data", data);
        }

        return errorAttributes;
    }

    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    @NonNull
    @Override
    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        return super.renderErrorResponse(request);
    }

    @Override
    protected HttpStatus getHttpStatus(Map<String, Object> errorAttributes) {
        return HttpStatus.OK;
    }
}
