package net.guerlab.spring.webflux.exception;

import net.guerlab.spring.commons.exception.AbstractI18nInfo;
import net.guerlab.spring.commons.exception.Keys;

import java.util.Locale;

/**
 * 未发现处理程序(404)
 *
 * @author guer
 */
public class NoHandlerFoundExceptionInfo extends AbstractI18nInfo {

    private static final String DEFAULT_MSG;

    static {
        Locale locale = Locale.getDefault();

        if (Locale.CHINA.equals(locale)) {
            DEFAULT_MSG = "请求地址无效[%s %s]";
        } else {
            DEFAULT_MSG = "Invalid request address[%s %s]";
        }
    }

    private String method;

    private String url;

    /**
     * 初始化
     *
     * @param cause
     *         原始异常
     * @param method
     *         请求方法
     * @param url
     *         请求地址
     */
    public NoHandlerFoundExceptionInfo(Throwable cause, String method, String url) {
        super(cause, 404);
        this.method = method;
        this.url = url;
    }

    @Override
    protected String getKey() {
        return Keys.NO_HANDLER_FOUND;
    }

    @Override
    protected Object[] getArgs() {
        return new Object[] { method, url };
    }

    @Override
    protected String getDefaultMessage() {
        return String.format(DEFAULT_MSG, method, url);
    }
}
